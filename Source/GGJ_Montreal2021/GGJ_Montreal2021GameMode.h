// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GGJ_Montreal2021GameMode.generated.h"

UCLASS(minimalapi)
class AGGJ_Montreal2021GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGGJ_Montreal2021GameMode();
};



